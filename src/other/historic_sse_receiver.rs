use eventsource_client::Client;
use poise::serenity_prelude::{
    colours::{
        branding::{GREEN, RED},
        roles::BLUE,
    },
    futures::TryStreamExt,
    CacheHttp, Context,
};
use serde::{Deserialize, Serialize};
use tracing::{error, info};

use crate::data::Environment;

#[derive(Serialize, Deserialize)]
struct SseEnqueuedPayload {
    queue_position: usize,
}

#[derive(Serialize, Deserialize)]
struct SseFinishedPayload {
    created: u32,
    ignored: u32,
    updated: u32,
}

#[derive(Serialize, Deserialize)]
enum SsePayload {
    EnqueueRejected,
    Enqueued(SseEnqueuedPayload),
    Started,
    Finished(SseFinishedPayload),
}

#[derive(Serialize, Deserialize)]
struct SseEvent {
    discord_id: i64,
    job_id: String,
    payload: SsePayload,
}

pub(crate) async fn historic_sse_listener_loop(ctx: Context) {
    let token = Environment::server_auth();
    let endpoint = format!(
        "{}/admin/sse/elite-historic-aggregate",
        Environment::server_address()
    );

    let client = eventsource_client::ClientBuilder::for_url(&endpoint)
        .unwrap()
        .header("Authorization", &format!("Bearer {}", token))
        .unwrap()
        .build();

    let mut stream = client
        .stream()
        .map_ok(|event| match event {
            eventsource_client::SSE::Event(event) => {
                let event_str = event.data;

                let event: Result<SseEvent, serde_json::Error> = serde_json::from_str(&event_str);

                match event {
                    Ok(payload) => {
                        info!(
                            "RX Event for User {:?} with ID {}",
                            payload.discord_id, payload.job_id
                        );

                        tokio::spawn(send_dm_for_event(payload, ctx.clone()));
                    }
                    Err(err) => {
                        error!("Failed to parse inbound SSE Event as Payload: {}", err)
                    }
                }
            }
            eventsource_client::SSE::Comment(_) => {}
        })
        .map_err(|err| error!("Failed to RX SSE event: {}", err));
    info!("Set up SSE Listener for aggregate events.");
    while let Ok(Some(_)) = stream.try_next().await {}
}

async fn send_dm_for_event(event: SseEvent, ctx: Context) {
    let user = match ctx.http().get_user(event.discord_id as u64).await {
        Ok(user) => user,
        Err(err) => {
            error!("Failed to get user {}. Reason: {}", event.discord_id, err);
            return;
        }
    };

    let colour = match event.payload {
        SsePayload::EnqueueRejected => RED,
        SsePayload::Enqueued(_) => BLUE,
        SsePayload::Started => BLUE,
        SsePayload::Finished(_) => GREEN,
    };

    let title = match event.payload {
        SsePayload::EnqueueRejected => "Background Upload Task rejected",
        SsePayload::Enqueued(_) => "Background Upload enqueued",
        SsePayload::Started => "Background Upload started",
        SsePayload::Finished(_) => "Background Upload finished",
    };

    let description = match event.payload {
        SsePayload::EnqueueRejected => "Could not enqueue your job. This could be because the Queue is full. Please try again in a couple minutes. If it still doesn't work, please send a message to <@220248762965164032>.".to_string(),
        SsePayload::Enqueued(data) => format!("Your job has been enqueued and is in position {}", data.queue_position),
        SsePayload::Started => "The Background Task has started handling your upload job.".to_string(),
        SsePayload::Finished(event) => format!("Here is your summary:\n- {} × **Added** (new event)\n- {} × **Updated** (new information, already known event)\n- {} × **Ignored** (no new info, already known event)", event.created, event.updated, event.ignored),
    };

    if let Err(err) = user
        .direct_message(ctx.http(), |x| {
            x.add_embed(|e| {
                e.color(colour)
                    .title(title)
                    .footer(|x| x.text(format!("Job ID: {}", event.job_id)))
                    .description(description)
            })
        })
        .await
    {
        error!("Failed to send historic summary to uploader: {}", err);
    } else {
        info!(
            "Sent an SSE Status Message of type '{}' to user {}",
            title, user.name
        );
    }
}
