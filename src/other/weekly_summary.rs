use poise::serenity_prelude::futures::TryFutureExt;
use reqwest::StatusCode;
use serde::{Deserialize, Serialize};

#[derive(Debug, Serialize, Deserialize)]
pub(crate) struct WeeklyLeaderboardEntry {
    pub(crate) cmdr_name: String,
    pub(crate) kills: Option<i64>,
}

pub async fn fetch_from_server(page: Option<u8>) -> Result<Vec<WeeklyLeaderboardEntry>, String> {
    let server_url = crate::data::Environment::server_address();
    let server_auth = crate::data::Environment::server_auth();

    let mut page = page.unwrap_or(1);
    if page == 0 {
        page = 1;
    }

    async fn run_request(
        path: String,
        server_auth: String,
    ) -> Result<Vec<WeeklyLeaderboardEntry>, String> {
        let response = reqwest::Client::new()
            .get(path)
            .bearer_auth(server_auth)
            .send()
            .map_err(|x| x.to_string())
            .await?;

        if response.status() != StatusCode::OK {
            return Err(format!(
                "Backend responded with Status {}",
                response.status().as_str()
            ));
        }
        let parsed_response: Vec<WeeklyLeaderboardEntry> = response
            .json::<Vec<WeeklyLeaderboardEntry>>()
            .await
            .map_err(|x| x.to_string())?;
        Ok(parsed_response)
    }

    run_request(
        format!("{server_url}/elite/leaderboard/weekly?page={page}&size=20"),
        server_auth,
    )
    .await
}
