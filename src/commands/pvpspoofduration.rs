use poise::serenity_prelude::{Color, GuildId, RoleId};
use reqwest::StatusCode;
use serde::{Deserialize, Serialize};
use tracing::{error, info};

use super::super::{Context, Error};
use crate::data::Environment;

/// Define how many minutes a live kill was submitted a wrong system location and victim ship is shown
#[poise::command(slash_command)]
pub async fn pvpspoofduration(
    ctx: Context<'_>,
    #[description = "0 = disabled, anything else in Minutes."]
    #[min = 0]
    #[max = 1440]
    duration_minutes: u16,
) -> Result<(), Error> {
    let user = ctx.author();

    // First Check if the user has the relevant role
    let required_role_id = RoleId(Environment::role_auth());
    let required_guild_id = GuildId(Environment::guild_id());

    if !user
        .has_role(ctx.http(), required_guild_id, required_role_id)
        .await?
    {
        // User does not have the needed Role
        ctx.send(|b| b.ephemeral(true).embed(|e| {
            e.color(Color::RED)
                .title(":octagonal_sign: No Permission")
                .description(format!("You don't have permissions to get an API Key. Gank some plebs to get the perms needed. \n\n# [Further infos]({}/elite/submit)", Environment::ui_baseurl()))
        })).await?;
        return Ok(());
    }
    // If here, user has permission to call this endpoint.
    // Could still be that they have perms, but never got an API Key, hence no account is present
    // We just try to patch. The Backend will complain if the user does not exist :)
    let discord_id = user.id.0;
    let response = patch_request(discord_id, duration_minutes).await;
    let (color, title) = match response {
        PatchResult::Done => (Color::DARK_GREEN, "Updated successfully".to_string()),
        PatchResult::NotFound => (Color::ORANGE, "Account not found".to_string()),
        PatchResult::Error(_) => (Color::RED, "Something broke".to_string()),
    };

    ctx.send(|b| {
        b.ephemeral(true).embed(|e| {
            let e = e.color(color).title(title);
            if let PatchResult::Error(x) = response {
                e.description(x);
            }
            e
        })
    })
    .await?;

    Ok(())
}

enum PatchResult {
    Done,
    NotFound,
    Error(String),
}

async fn patch_request(discord_id: u64, duration: u16) -> PatchResult {
    let server_url = crate::data::Environment::server_address();
    let server_auth = crate::data::Environment::server_auth();

    #[derive(Deserialize, Serialize)]
    struct Body {
        spoof_location_duration_minutes: u16,
    }

    let body = Body {
        spoof_location_duration_minutes: duration,
    };

    let path = format!("{}/admin/users/{}", server_url, discord_id);
    match reqwest::Client::new()
        .patch(path)
        .json(&body)
        .bearer_auth(server_auth)
        .send()
        .await
    {
        Ok(x) => match x.status() {
            StatusCode::OK => {
                info!("Patches successfully!");
                PatchResult::Done
            }
            StatusCode::NOT_FOUND => PatchResult::NotFound,
            _ => {
                error!(
                    "unexpected Status Code when patching spoof duration to {} for account {}",
                    &duration, &discord_id
                );
                PatchResult::Error(format!(
                    "Unexpected Status Code {}. If this persists, let wdx know.",
                    x.status()
                ))
            }
        },
        Err(err) => {
            let is_404_issue = err
                .status()
                .map(|x| x == StatusCode::NOT_FOUND)
                .unwrap_or(false);
            if is_404_issue {
                PatchResult::NotFound
            } else {
                error!(
                    "Error trying to patch account {} to set spoof duration to {}",
                    &discord_id, &duration
                );
                PatchResult::Error(
                    "Failed to patch spoof duration. If this error persists, contact wdx"
                        .to_string(),
                )
            }
        }
    }
}
