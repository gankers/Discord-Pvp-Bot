#[derive(Serialize, Deserialize, Debug)]
struct OkResponse {
    error: Option<String>,
    #[serde(rename = "isBanned")]
    is_banned: Option<bool>,
    token: Option<String>,
}

use std::time::Duration;

use poise::serenity_prelude::{Color, GuildId, RoleId};
use reqwest::StatusCode;
use serde::{Deserialize, Serialize};
use tracing::info;

use crate::data::Environment;

use super::super::{Context, Error};

/// Request an API Token. You can also use this command to get
#[poise::command(slash_command)]
pub async fn pvpregister(ctx: Context<'_>) -> Result<(), Error> {
    let user = ctx.author();

    // First Check if the user has the relevant role
    let required_role_id = RoleId(Environment::role_auth());
    let required_guild_id = GuildId(Environment::guild_id());

    if !user
        .has_role(ctx.http(), required_guild_id, required_role_id)
        .await?
    {
        // User does not have the needed Role
        ctx.send(|b| b.embed(|e| {
            e.color(Color::RED)
                .title(":octagonal_sign: No Permission")
                .description(format!("You don't have permissions to get an API Key. Gank some plebs to get the perms needed. \n\n# [Further infos]({}/elite/submit)", Environment::ui_baseurl()))
        })).await?;
        return Ok(());
    }

    let discord_id = user.id.0;

    info!("User {} requested a new token via /pvpregister", user.name);

    let client = reqwest::Client::new();

    // First, check if the user exists
    let exists_response = client
        .get(format!(
            "{}/admin/users/{}",
            Environment::server_address(),
            user.id.0
        ))
        .bearer_auth(Environment::server_auth())
        .send()
        .await?;

    if exists_response.status() == StatusCode::NOT_FOUND {
        // Does not exist yet!
        #[derive(Serialize, Deserialize)]
        struct RegisterAccountBody {
            discord_id: u64,
        }

        let create_response = client
            .post(format!("{}/admin/users", Environment::server_address(),))
            .bearer_auth(Environment::server_auth())
            .json(&RegisterAccountBody { discord_id })
            .send()
            .await?;

        if create_response.status() != StatusCode::OK {
            return Err(Error::from(format!(
                "Failed to create Account. Server responded with Status {}",
                create_response.status()
            )));
        }
    } else {
        // Account already exists. Might be banned?
        #[derive(Debug, Deserialize, Serialize)]
        pub(crate) struct AccountResponse {
            is_banned: bool,
        }

        let AccountResponse { is_banned } = exists_response.json::<AccountResponse>().await?;
        if is_banned {
            ctx.send(|b| {
                b.ephemeral(true).embed(|e| {
                    e.color(Color::RED)
                        .title(":white_check_mark: Account banned")
                        .description("Your account is disabled. No new tokens can be issues.")
                })
            })
            .await?;
            return Ok(());
        }
    }

    // If here, an account was created! We can POST a new auto-expire Token.
    #[derive(Debug, Deserialize, Serialize)]
    pub(crate) struct Permissions {
        admin: bool,
        self_api: bool,
        elite_submit: bool,
    }

    #[derive(Debug, Deserialize, Serialize)]
    pub(crate) struct TokenData {
        owner_discord_id: u64,
        revoke_on_new_token: bool,
        revoked: bool,
        permissions: Permissions,
    }

    let token_data = TokenData {
        owner_discord_id: discord_id,
        revoke_on_new_token: true,
        revoked: false,
        permissions: Permissions {
            admin: false,
            self_api: true,
            elite_submit: true,
        },
    };

    let raw_response = client
        .post(format!(
            "{}/admin/users/{}/tokens",
            Environment::server_address(),
            discord_id
        ))
        .bearer_auth(Environment::server_auth())
        .json(&token_data)
        .send()
        .await?;

    #[derive(Debug, Deserialize, Serialize)]
    pub(crate) struct TokenWithData {
        pub(crate) token: String,
        pub(crate) data: TokenData,
    }

    let response = raw_response
        .error_for_status()?
        .json::<TokenWithData>()
        .await?;

    // Got a valid token from the Server
    let sleep_duration_seconds = 60;
    let message = ctx
        .send(|b| {
            b.ephemeral(true).embed(|e| {
                e.color(Color::DARK_GREEN)
                    .title(":white_check_mark: New API Key created")
                    .description(format!(
                        "You have {}s to copy the key.\n\nOnly you can see this message.",
                        sleep_duration_seconds
                    ))
                    .field("API Key", format!("`{}`", response.token), false)
            })
        })
        .await?;
    tokio::time::sleep(Duration::from_secs(sleep_duration_seconds)).await;
    message
        .edit(ctx, |f| {
            f.embed(|e| {
                e.title(":white_check_mark: New API Key created")
                    .color(Color::BLUE)
                    .description("Key has been hidden. Rerun `/pvpregister`, which will give you a new key. The old key will get invalidated.")
            })
        })
        .await?;

    Ok(())
}
