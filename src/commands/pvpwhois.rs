use chrono::{DateTime, Utc};
use poise::serenity_prelude::{
    futures::{stream, Stream},
    Color, CreateEmbed,
};
use reqwest::StatusCode;
use serde::{Deserialize, Serialize};
use tracing::error;
use urlencoding::encode;

use crate::{data::Environment, util::new_username::convert_tag_to_username};

use super::super::{Context, Error};

#[derive(Debug, Serialize, Deserialize)]
struct CmdrEntry {
    name: String,
    squadron: Option<String>,
    rank_id: u8,
    ship_id: Option<String>,
    ship_name: Option<String>,
}

#[derive(Debug, Serialize, Deserialize)]
struct CmdrWhoisLookupResponseEntry {
    killer: CmdrEntry,
    victim: CmdrEntry,
    kill_timestamp: DateTime<Utc>,
    system_name: Option<String>,
}

impl CmdrWhoisLookupResponseEntry {
    fn did_die(&self, cmdr: &str) -> bool {
        self.victim.name.to_lowercase() == cmdr.to_lowercase()
    }
}

async fn autocomplete_search<'a>(
    _ctx: Context<'_>,
    partial: &'a str,
) -> impl Stream<Item = String> + 'a {
    let server_url = crate::data::Environment::server_address();
    let server_auth = crate::data::Environment::server_auth();

    async fn run_request<'a>(
        path: String,
        server_auth: String,
        partial: &'a str,
    ) -> Option<Vec<String>> {
        #[derive(Serialize)]
        struct Params {
            q: String,
        }
        let response = reqwest::Client::new()
            .get(path)
            .query(&Params {
                q: partial.to_string(),
            })
            .bearer_auth(server_auth)
            .send()
            .await;

        match response {
            Ok(response_ok) => {
                if response_ok.status() != StatusCode::OK {
                    return None;
                }

                let parsed_response: Option<Vec<String>> =
                    match response_ok.json::<Vec<String>>().await {
                        Ok(data) => Some(data),
                        Err(_) => None,
                    };
                parsed_response
            }
            Err(err) => {
                eprintln!("Failed to query: {}", err);
                None
            }
        }
    }

    let response = match run_request(
        format!("{}/elite/search/cmdrs", server_url),
        server_auth,
        partial,
    )
    .await
    {
        Some(data) => data,
        None => vec![],
    };

    stream::iter(response)
}

#[derive(Debug, Serialize, Deserialize)]
struct CmdrWhoisLookupResponseSuccess {
    kills: u32,
    deaths: u32,
    page_data: Vec<CmdrWhoisLookupResponseEntry>,
    cmdr_name: String,
    squadron: Option<String>,
    page: u32,
    total_pages: u32,
}

impl CmdrWhoisLookupResponseSuccess {
    fn apply_to_embed(self, embed: &mut CreateEmbed, callee: String) -> &mut CreateEmbed {
        let historic_data: Vec<_> = self.page_data;
        let historic_data_len = historic_data.len();
        let mut cmdr_row: Vec<String> = vec![];
        let mut system_row: Vec<String> = vec![];
        let mut date_row: Vec<String> = vec![];

        let mut has_system_data = false;

        for entry in historic_data {
            let did_self_die = entry.did_die(&self.cmdr_name);
            let emoji_to_use = match did_self_die {
                false => ":dagger:",
                true => ":skull:",
            };
            let other_cmdr = match did_self_die {
                false => entry.victim,
                true => entry.killer,
            };

            cmdr_row.push(format!("{} {}", emoji_to_use, other_cmdr.name));
            let location_string = match entry.system_name {
                Some(location) => {
                    has_system_data = true;
                    location
                }
                None => "".to_string(),
            };
            system_row.push(location_string);
            date_row.push(entry.kill_timestamp.format("%Y-%m-%d").to_string());
        }

        let description_as_heading = match historic_data_len {
            0 => "",
            _ => "\n\n**__Recent History__**\n\n",
        };
        let squadron_prefix = match &self.squadron {
            Some(tag) => format!("[{}] ", tag),
            None => String::new(),
        };

        embed.title(format!("{}CMDR {}", squadron_prefix, &self.cmdr_name));
        embed.description(format!(
            " :dagger: × {} :skull: × {} {}",
            self.kills, self.deaths, description_as_heading
        ));
        if historic_data_len > 0 {
            embed.field(":busts_in_silhouette:", cmdr_row.join("\n"), true);
            if has_system_data {
                embed.field(":ringed_planet:", system_row.join("\n"), true);
            }
            embed.field(":calendar_spiral:", date_row.join("\n"), true);
        }

        embed.footer(|f| {
            f.text(format!(
                "Asked by {} · Page {} out of {}",
                callee, &self.page, &self.total_pages
            ))
        });
        let url = format!(
            "{}/elite/cmdrs/{}",
            Environment::ui_baseurl(),
            encode(&self.cmdr_name)
        );
        embed.url(url);

        embed
    }

    pub async fn get_from_server(cmdr_name: &str, page: &u32) -> Result<Option<Self>, String> {
        let safe_cmdr_name = urlencoding::encode(cmdr_name).into_owned();
        let server_url = crate::data::Environment::server_address();
        let server_auth = crate::data::Environment::server_auth();

        async fn run_request(
            path: String,
            server_auth: String,
            page: &u32,
        ) -> Result<Option<CmdrWhoisLookupResponseSuccess>, reqwest::Error> {
            let response = reqwest::Client::new()
                .get(path)
                .bearer_auth(server_auth)
                .query(&[("page_size", "20"), ("page", page.to_string().as_str())])
                .send()
                .await?;

            if response.status() == StatusCode::NOT_FOUND {
                return Ok(None);
            }

            let parsed_response: CmdrWhoisLookupResponseSuccess = response.json().await?;
            Ok(Some(parsed_response))
        }

        let response = run_request(
            format!("{}/elite/cmdrs/{}", server_url, safe_cmdr_name),
            server_auth,
            page,
        )
        .await;
        match response {
            Ok(good_response) => Ok(good_response),
            Err(bad_response) => {
                error!("Failed to parse Response from Whois: {}", bad_response);
                Err(bad_response.to_string())
            }
        }
    }
}

/// Look up a CMDR on the bot.
#[poise::command(slash_command)]
pub async fn pvpwhois(
    ctx: Context<'_>,
    #[autocomplete = "autocomplete_search"]
    #[description = "The CMDR name (without the CMDR Prefix)"]
    cmdr: String,
    #[description = "Which page to show? Default to first"] page: Option<u32>,
) -> Result<(), Error> {
    let response = CmdrWhoisLookupResponseSuccess::get_from_server(&cmdr, &page.unwrap_or(1)).await;

    ctx.send(|builder| {
        builder.embed(|embed| match response {
            Err(err) => embed.color(Color::RED).title("Error").description(err),
            Ok(data) => match data {
                Some(response) => response
                    .apply_to_embed(embed, convert_tag_to_username(ctx.author().tag()))
                    .color(Color::DARK_GREEN),
                None => embed
                    .color(Color::GOLD)
                    .title("Not Found")
                    .description(format!("We do not have any data for CMDR {}", &cmdr).as_str()),
            },
        })
    })
    .await
    .map_err(|x| {
        error!("Error sending pvpwhois Response: {}", &x);
        x
    })?;
    Ok(())
}
