use chrono::prelude::*;
use chrono::{DateTime, Duration, Utc};
use itertools::enumerate;
use poise::serenity_prelude::Color;

use crate::other::weekly_summary::fetch_from_server;

use super::super::{Context, Error};

pub struct DataRow {
    pub position: u32,
    pub cmdr: String,
    pub kill_count: u32,
}

/// Gets the leaderboard for current game week (ends on Thursdays 8AM UTC)
#[poise::command(slash_command)]
pub async fn pvpweekly(ctx: Context<'_>) -> Result<(), Error> {
    let response_from_backend = fetch_from_server(None).await;
    let time_range_utc = get_current_weekly_period_from_timestamp(Utc::now());
    let time_range_date = (
        time_range_utc.0.format("%Y-%m-%d"),
        time_range_utc.1.format("%Y-%m-%d"),
    );

    let message = ctx
        .send(|builder| {
            builder.embed(|embed: &mut poise::serenity_prelude::CreateEmbed| {
                match response_from_backend {
                    Err(err) => embed.color(Color::RED).title("Error").description(err),
                    Ok(data) => {
                        let result: Vec<(String, u32)> = data
                            .into_iter()
                            .map(|x| (x.cmdr_name, x.kills.unwrap_or(0) as u32))
                            .collect();

                        let data = result
                            .into_iter()
                            .enumerate()
                            .map(|(i, e)| DataRow {
                                position: u32::try_from(i).unwrap() + 1,
                                cmdr: e.0.to_owned(),
                                kill_count: e.1,
                            })
                            .collect::<Vec<_>>();

                        build_weekly_embeds(data, embed);

                        embed.footer(|footer| {
                            footer.text(format!(
                                "Current leaderboard period is from {} to {}, starting at 8AM UTC.",
                                time_range_date.0, time_range_date.1
                            ))
                        });

                        embed
                    }
                }
            })
        })
        .await;

    if let Err(reason) = message {
        println!("Error Sending Message: {:?}", reason)
    }

    Ok(())
}

fn get_next_weekly_emission_time(now: DateTime<Utc>) -> DateTime<Utc> {
    let days_until_next_thursday = match now.weekday() {
        chrono::Weekday::Thu => match now.hour() >= 8 {
            true => 7,
            false => 0,
        },
        other => {
            let mut diff = chrono::Weekday::Thu.num_days_from_monday() as i64
                - other.num_days_from_monday() as i64;

            if diff < 0 {
                diff += 7;
            }

            diff
        }
    };

    (now + Duration::days(days_until_next_thursday))
        .with_hour(8)
        .unwrap()
        .with_minute(0)
        .unwrap()
        .with_second(0)
        .unwrap()
}

fn get_current_weekly_period_from_timestamp(time: DateTime<Utc>) -> (DateTime<Utc>, DateTime<Utc>) {
    // Get the "next" thursday
    let upper = get_next_weekly_emission_time(time);
    let lower = upper - Duration::days(7);

    let delta = time - lower;

    if delta <= Duration::minutes(5) {
        return (lower - Duration::days(7), upper - Duration::days(7));
    }

    (lower, upper)
}

fn build_weekly_embeds(
    killer_and_count: Vec<DataRow>,
    embed: &mut poise::serenity_prelude::CreateEmbed,
) {
    if killer_and_count.is_empty() {
        embed
            .color(Color::DARK_RED)
            .title("No Kills in this Time Period… yet.");
        return;
    }

    let time_range = get_current_weekly_period_from_timestamp(Utc::now());

    embed
        .color(Color::DARK_GREEN)
        .title("Weekly Leaderboard Summary")
        .description("[View the full weekly board](https://gankers.org/elite/weekly)");

    let mut left_row = Vec::with_capacity(killer_and_count.len());
    let mut center_row = Vec::with_capacity(killer_and_count.len());
    let mut right_row = Vec::with_capacity(killer_and_count.len());

    // if multiple CMDRs have the same amount, they should have the same Position in the board.
    let mut last_kill_count = 0;
    let mut last_position = 0;
    for (idx, entry) in enumerate(killer_and_count) {
        let position_to_display = {
            if last_kill_count == entry.kill_count {
                last_position
            } else {
                last_kill_count = entry.kill_count;
                last_position = (idx as i32) + 1;
                last_position
            }
        };
        let escaped_cmdr = entry.cmdr.replace('\\', "\\\\");

        left_row.push(format!("{}", position_to_display));
        center_row.push(escaped_cmdr);
        right_row.push(format!(
            "[{}](https://gankers.org/elite/cmdrs/{})",
            entry.kill_count,
            urlencoding::encode(&entry.cmdr)
        ));
    }

    let footer = format!(
        "Summary for {} until {} 8AM UTC",
        time_range.0.format("%Y-%m-%d"),
        time_range.1.format("%Y-%m-%d")
    );

    let mut right_row = right_row.join("\n");
    if right_row.len() > 1000 {
        right_row.replace_range(1000.., "\n...");
    }
    let mut center_row = center_row.join("\n");
    if center_row.len() > 1000 {
        center_row.replace_range(1000.., "\n...");
    }

    embed
        .field(":trophy:", left_row.join("\n"), true)
        .field(":busts_in_silhouette:", center_row, true)
        .field("×:dagger:", right_row, true)
        .footer(|x| x.icon_url("https://gankers.org/og.png").text(footer));
}
