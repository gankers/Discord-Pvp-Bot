use reqwest::StatusCode;
use serde::{Deserialize, Serialize};

pub(crate) async fn has_token_correct_perms() -> Result<(), String> {
    let server_url = crate::data::Environment::server_address();
    let server_auth = crate::data::Environment::server_auth();

    #[derive(Serialize, Deserialize)]
    struct ReponsePermissions {
        admin: bool,
        self_api: bool,
        elite_submit: bool,
    }

    #[derive(Serialize, Deserialize)]
    struct ResponsePayload {
        revoked: bool,
        permissions: ReponsePermissions,
    }

    let response = reqwest::Client::new()
        .get(format!("{}/users/self/tokens/active", server_url))
        .bearer_auth(server_auth)
        .send()
        .await;

    let payload_from_request = match response {
        Ok(response_ok) => {
            if response_ok.status() != StatusCode::OK {
                return Err(format!(
                    "Expected Status OK but received {}",
                    response_ok.status()
                ));
            }

            match response_ok.json::<ResponsePayload>().await {
                Ok(data) => data,
                Err(err) => return Err(err.to_string()),
            }
        }
        Err(err) => return Err(err.to_string()),
    };

    if payload_from_request.revoked {
        return Err(String::from("Used admin token is revoked. Cannot continue"));
    }

    if !payload_from_request.permissions.admin {
        return Err(String::from("Provided Token does not have Admin Permissions. Please replace with Admin token. Cannot continue."));
    }

    Ok(())
}
