pub fn convert_tag_to_username(username: String) -> String {
   
    let segment =  username.split('#').find(|_x| true);
    match segment {
        Some(e) => e.to_string(),
        None => username
    }
}