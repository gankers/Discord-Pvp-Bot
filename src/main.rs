mod commands;
mod data;
mod other;
mod util;
use commands::{
    pvpregister::pvpregister, pvpspoofduration::pvpspoofduration, pvpweekly::pvpweekly,
    pvpwhois::pvpwhois,
};
use other::historic_sse_receiver::historic_sse_listener_loop;
use poise::serenity_prelude as serenity;
use tracing::{error, info};
use util::startup_check::has_token_correct_perms;

pub struct Data {}
pub type Error = Box<dyn std::error::Error + Send + Sync>;
pub type Context<'a> = poise::Context<'a, Data, Error>;

#[tokio::main]
async fn main() {
    // This will panic and fail is there is an Error Return value.
    data::startup_check().unwrap();

    tracing_subscriber::fmt()
        .with_max_level(tracing::Level::INFO)
        .init();

    let framework = poise::Framework::builder()
        .options(poise::FrameworkOptions {
            commands: vec![pvpspoofduration(), pvpwhois(), pvpregister(), pvpweekly()],
            ..Default::default()
        })
        .token(data::Environment::discord_token())
        .intents(serenity::GatewayIntents::non_privileged())
        .setup(|ctx, _ready, framework| {
            Box::pin(async move {
                let guild_id = serenity::GuildId(data::Environment::guild_id());
                poise::builtins::register_in_guild(ctx, &framework.options().commands, guild_id)
                    .await?;

                // Historic SSE Listener
                tokio::spawn(historic_sse_listener_loop(ctx.clone()));

                Ok(Data {})
            })
        });

    // Do an Auth Check. If the provided Token does NOT have needed perms, there is no point in continuing.
    match has_token_correct_perms().await {
        Ok(_) => info!("Auth Check passed. Admin Token accepted by Backend."),
        Err(err) => {
            error!("Auth Check failed. Admin Token was rejected by Backend because of the following issue: {}", err);
            panic!("Auth rejected by backend.")
        }
    }

    println!("Starting up Bot...");
    framework.run().await.unwrap();
}
